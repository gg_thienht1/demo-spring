package com.example.demo.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.demo.entity.BookEntity;
import com.example.demo.repository.BookRepository;



@Controller
public class BookController {
	@Autowired
	BookRepository bookRepository;
	@RequestMapping(value="/")
	public String homeController(Model model) {
		List<BookEntity> bookEntities= (List<BookEntity>) bookRepository.findAll();
		model.addAttribute("book", bookEntities);
		return "index";
	}
	   @RequestMapping(value = "/search")
	    public String search(@RequestParam("search")String search, 
	    		Model model){
	        List<BookEntity> result;
	        if(search.isEmpty()){
	            result= (List<BookEntity>)bookRepository.findAll();
	        }
	        else {
	            result=bookRepository.findByNameBook(search);
	        }
	        model.addAttribute("book",result);
	        return "index";
	        }
	@RequestMapping(value="/addBook")
	public String addBook(Model model) {
		model.addAttribute("book", new BookEntity());
		model.addAttribute("action", "new");
		return "addBook";
	}
	
	@RequestMapping(value="new",produces = "text/plain;charset=UTF-8")
	public String newBook( BookEntity book) {
		bookRepository.save(book);
		return "redirect:/";
	}
	@RequestMapping(value="/delete")
	public String deleteBook(BookEntity book) {
		bookRepository.delete(book);
		return "redirect:/";
	}
	@RequestMapping(value="/editBook")
	public String editBook(String id,Model model, BookEntity book) {
		model.addAttribute("bookUpdate", book);
		model.addAttribute("action", "update");
		return "editBook";
	}
	
	@RequestMapping(value="/update")
	public String updateBook(BookEntity book) {
		bookRepository.save(book);
		return "redirect:/";
	}
}
