package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.BookEntity;

@Repository
public interface BookRepository extends CrudRepository<BookEntity, Integer>{

    List<BookEntity> findByNameBook(String name);
}
