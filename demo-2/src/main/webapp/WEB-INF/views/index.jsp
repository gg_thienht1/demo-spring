<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>index</title>
</head>
<body>
<h1>book</h1>
<form action="/search" method="post">
<input type="text" name="search"/>
<input type="submit" value="seach">
</form>
<a href="/addBook">add</a>
<table border="1">
	<tr>
		<th>Name Book</th>
		<th>Author</th>
		<th>Description</th>
		<td>edit</td>
		<td>delete</td>
	</tr>
	<c:forEach items="${book}" var="bookList">
		<tr>
			<td>${bookList.nameBook}</td>
			<td>${bookList.author}</td>
			<td>${bookList.description}</td>
			<td><a href="/editBook?id=${bookList.id}">edit</a></td>
			<td><a href="/delete?id=${bookList.id}">delete</a></td>
		</tr>
	</c:forEach>
</table>
</body>
</html>